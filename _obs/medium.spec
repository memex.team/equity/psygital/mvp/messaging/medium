%global _obs_path %(ls -d %{_sourcedir}/*.obs)
%global _obs_helper     %(echo %{_obs_path}|sed -e "s/.*\\///")

%global _my_release_name    %(echo %{_obs_helper}|sed -e "s/-__.*//")
%global _my_release_custname    %(echo %{_obs_helper}|sed -e "s/.*__CustName@//" -e "s/__.*//")
%global _my_release_version %(echo %{_obs_helper}|sed -e "s/.*__Ver@//" -e "s/__.*//")
%global _my_release_date    %(echo %{_obs_helper}|sed -e "s/.*__RelDate@//" -e "s/__.*//")
%global _my_release_tag     %(echo %{_obs_helper}|sed -e "s/.*__Hash@//" -e "s/__.*//")

%define __builder ninja
%bcond_without  fixed_gcc

#MANDATORY FIELDS
Version: %{_my_release_version}
Name:    %{_my_release_name}
Release: 1
## \\ In4 //

Summary:        Messaging application with a focus on speed and security
License:        GPL-3.0-only
Group:          Productivity/Networking/Instant Messenger
URL:            https://github.com/telegramdesktop/tdesktop
# PATCH-FIX-OPENSUSE
Patch1:         0001-use-bundled-ranged-exptected-gsl.patch
# PATCH-FIX-OPENSUSE
BuildRequires:  cmake(Qt5Core)
BuildRequires:  cmake(Qt5DBus)
BuildRequires:  cmake(Qt5Gui)
BuildRequires:  cmake(Qt5Network)
BuildRequires:  cmake(Qt5Widgets)
BuildRequires:  cmake(KF5Wayland)
BuildRequires:  cmake(Qt5WaylandClient)
BuildRequires:  cmake(Qt5XkbCommonSupport)
BuildRequires:  cmake(dbusmenu-qt5)
BuildRequires:  cmake(range-v3)
BuildRequires:  cmake(tg_owt)
BuildRequires:  cmake(Microsoft.GSL)
BuildRequires:  appstream-glib
BuildRequires:  chrpath
BuildRequires:  cmake >= 3.16
BuildRequires:  extra-cmake-modules
BuildRequires:  desktop-file-utils
BuildRequires:  enchant-devel
BuildRequires:  ffmpeg-devel
BuildRequires:  freetype-devel
BuildRequires:  wayland-devel
BuildRequires:  qrcodegen-devel
BuildRequires:  rnnoise-devel
BuildRequires:  libwebrtc_audio_processing-devel
%if %{with fixed_gcc}
BuildRequires:  gcc10-c++
%else
BuildRequires:  gcc-c++
%endif
BuildRequires:  expected-devel
BuildRequires:  glibc-devel
BuildRequires:  libQt5Core-private-headers-devel
BuildRequires:  libQt5Gui-private-headers-devel
BuildRequires:  libjpeg-devel
BuildRequires:  liblz4-devel
BuildRequires:  wayland-devel
BuildRequires:  libqt5-qtwayland
BuildRequires:  libqt5-qtwayland-devel
BuildRequires:  libqt5-qtbase-common-devel
BuildRequires:  libqt5-qtimageformats-devel
BuildRequires:  libqt5-qtwayland-private-headers-devel
BuildRequires:  libqt5-qtbase-private-headers-devel
BuildRequires:  rnnoise-devel
BuildRequires:  memory-constraints
BuildRequires:  ninja
BuildRequires:  ccache
BuildRequires:  pkgconfig
BuildRequires:  xorg-x11-devel
BuildRequires:  xxhash-devel
BuildRequires:  unzip
BuildRequires:  xz
BuildRequires:  pkgconfig(alsa)
BuildRequires:  pkgconfig(openal)
BuildRequires:  pkgconfig(expat)
BuildRequires:  pkgconfig(fontconfig)
BuildRequires:  pkgconfig(freetype2)
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gtk+-3.0)
BuildRequires:  pkgconfig(glibmm-2.4)
BuildRequires:  pkgconfig(gdk-pixbuf-2.0)
BuildRequires:  pkgconfig(harfbuzz)
BuildRequires:  pkgconfig(hunspell)
BuildRequires:  pkgconfig(libavcodec)
BuildRequires:  pkgconfig(libavdevice)
BuildRequires:  pkgconfig(libavfilter)
BuildRequires:  pkgconfig(libavformat) >= 58
BuildRequires:  pkgconfig(libavutil)
BuildRequires:  pkgconfig(libcrypto)
BuildRequires:  pkgconfig(liblzma)
BuildRequires:  pkgconfig(libmng)
BuildRequires:  pkgconfig(libpcre)
BuildRequires:  pkgconfig(libpcre16)
BuildRequires:  pkgconfig(libpcrecpp)
BuildRequires:  pkgconfig(libpcreposix)
BuildRequires:  pkgconfig(libpng)
BuildRequires:  pkgconfig(libproxy-1.0)
BuildRequires:  pkgconfig(libpulse)
BuildRequires:  pkgconfig(libtiff-4)
BuildRequires:  pkgconfig(libva)
BuildRequires:  pkgconfig(libva-glx)
BuildRequires:  pkgconfig(libva-x11)
BuildRequires:  pkgconfig(libwebp)
BuildRequires:  pkgconfig(minizip)
BuildRequires:  pkgconfig(mtdev)
BuildRequires:  pkgconfig(openal)
BuildRequires:  pkgconfig(openssl)
BuildRequires:  pkgconfig(opus)
BuildRequires:  pkgconfig(opusfile)
BuildRequires:  pkgconfig(opusurl)
BuildRequires:  pkgconfig(portaudio-2.0)
BuildRequires:  pkgconfig(portaudiocpp)
BuildRequires:  pkgconfig(tslib)
BuildRequires:  pkgconfig(vdpau)
BuildRequires:  pkgconfig(xcb)
BuildRequires:  pkgconfig(xcb-record)
BuildRequires:  pkgconfig(xcb-screensaver)
BuildRequires:  pkgconfig(xcb-ewmh)
BuildRequires:  pkgconfig(xcb-icccm)
BuildRequires:  pkgconfig(xcb-image)
BuildRequires:  pkgconfig(xcb-keysyms)
BuildRequires:  pkgconfig(xcb-renderutil)
BuildRequires:  pkgconfig(xcb-util)
BuildRequires:  pkgconfig(xfixes)
BuildRequires:  pkgconfig(xkbcommon)
BuildRequires:  pkgconfig(xkbcommon-x11)
BuildRequires:  pkgconfig(zlib)
BuildRequires:  pkgconfig(libpulse)
BuildRequires:  pkgconfig(libturbojpeg)
BuildRequires:  pkgconfig(protobuf)
BuildRequires:  pkgconfig(webkit2gtk-4.0)
BuildRequires:  pkgconfig(libpipewire-0.3)
BuildRequires:  pkgconfig(xtst)
BuildRequires:  pkgconfig(jemalloc)
BuildRequires:  yasm
# Runtime requirements
Requires:       ffmpeg
Requires:       hicolor-icon-theme
Requires:       icu
Requires:       openssl
# TDesktop can fall back to a simple GTK file picker but prefers the portal
Recommends:     xdg-desktop-portal
ExclusiveArch:  x86_64
Conflicts:      libqt5-qtstyleplugins-platformtheme-gtk2

%description
Telegram is a non-profit cloud-based instant messaging service.
Users can send messages and exchange photos, videos, stickers, audio and files of any type.
Its client-side code is open-source software but the source code for recent versions is not
always immediately published, whereas its server-side code is closed-source and proprietary.
The service also provides APIs to independent developers.

%prep
## // In4 \\
mv %{_obs_path} %{_builddir}/medium-%{version} && cd %{_builddir}/medium-%{version}
## \\ In4 //
# %patch1 -p2

%build
%limit_build -m 2048
%if %{with fixed_gcc}
export CC="/usr/bin/gcc-10"
export CXX="/usr/bin/g++-10"
%endif


cd %{_builddir}/%{name}-%{version}
%cmake \
      -DCMAKE_INSTALL_PREFIX=%{_prefix} \
      -DCMAKE_BUILD_TYPE=Release \
      -DTDESKTOP_API_ID=447053 \
      -DTDESKTOP_API_HASH=19c47b7d1c89ceef4cf113d97c737598 \
      -DDESKTOP_APP_USE_GLIBC_WRAPS:BOOL=OFF \
      -DDESKTOP_APP_USE_PACKAGED:BOOL=ON \
      -DDESKTOP_APP_USE_PACKAGED_GSL:BOOL=ON \
      -DDESKTOP_APP_USE_PACKAGED_RLOTTIE:BOOL=OFF \
      -DDESKTOP_APP_LOTTIE_USE_CACHE:BOOL=OFF \
      -DDESKTOP_APP_QTWAYLANDCLIENT_PRIVATE_HEADERS:BOOL=ON \
      -DDESKTOP_APP_USE_PACKAGED_FONTS:BOOL=ON \
      -DDESKTOP_APP_DISABLE_CRASH_REPORTS:BOOL=ON \
      -DTDESKTOP_DISABLE_AUTOUPDATE:BOOL=ON \
      -DTDESKTOP_LAUNCHER_BASENAME=%{name} \
      -DDESKTOP_APP_DISABLE_WEBRTC_INTEGRATION:BOOL=OFF \
      -DDESKTOP_APP_DISABLE_GTK_INTEGRATION:BOOL=OFF \
      -DDESKTOP_APP_DISABLE_WAYLAND_INTEGRATION:BOOL=OFF \
      -DDESKTOP_APP_DISABLE_X11_INTEGRATION:BOOL=OFF
      

%cmake_build

%install
## // In4 \\
cd %{_builddir}/%{name}-%{version}
## \\ In4 //
%cmake_install

appstream-util validate-relax --nonet %{buildroot}%{_datadir}/metainfo/*.appdata.xml
mv %{buildroot}/%{_bindir}/telegram-desktop %{buildroot}/%{_bindir}/%{name}

%files
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor
%{_datadir}/icons/hicolor/*/apps/*.png
%{_datadir}/metainfo/*.appdata.xml

%changelog
